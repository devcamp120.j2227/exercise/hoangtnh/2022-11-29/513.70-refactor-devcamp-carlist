import gJsonCars from "./info";
function App() {

  const gCars = JSON.parse(gJsonCars);
  return (
    <div>
    <ul>{gCars.map(function(car, index){
      return <li key={index}>Car Name: {car.make} - vID: {car.vID}. This is {car.year <= 2018 ? "old car" : "new car"}</li>
    })}
    </ul>
  </div>

  );
}

export default App;
